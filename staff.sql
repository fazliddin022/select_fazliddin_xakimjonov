WITH StaffRevenue2017 AS (
    SELECT
        s.store_id,
        s.staff_id,
        CONCAT(s.first_name, ' ', s.last_name) AS staff_name,
        EXTRACT(YEAR FROM p.payment_date) AS payment_year,
        SUM(p.amount) AS total_revenue
    FROM
        payment p
    JOIN
        staff s ON p.staff_id = s.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id, s.staff_id, staff_name, payment_year
)
, RankedStaffRevenue AS (
    SELECT
        store_id,
        payment_year,
        staff_id,
        staff_name,
        total_revenue,
        RANK() OVER (PARTITION BY store_id, payment_year ORDER BY total_revenue DESC) AS revenue_rank
    FROM
        StaffRevenue2017
)
SELECT
    store_id,
    payment_year,
    staff_id,
    staff_name,
    total_revenue
FROM
    RankedStaffRevenue
WHERE
    revenue_rank = 1;

