WITH ActorGaps AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        MAX(f.release_year) - MIN(f.release_year) AS acting_gap
    FROM
        actor a
    JOIN
        film_actor fa ON a.actor_id = fa.actor_id
    JOIN
        film f ON fa.film_id = f.film_id
    GROUP BY
        a.actor_id, a.first_name, a.last_name
)
, LongestGapActors AS (
    SELECT
        actor_id,
        first_name,
        last_name,
        acting_gap,
        RANK() OVER (ORDER BY acting_gap DESC) AS gap_rank
    FROM
        ActorGaps
)
SELECT
    first_name || ' ' || last_name AS "Actor/Actress",
    acting_gap AS "Acting Gap (Years)"
FROM
    LongestGapActors
WHERE
    gap_rank = 1;