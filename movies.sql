WITH TopRentedMovies AS (
    SELECT
        film.film_id,
        film.title,
        COUNT(rental.rental_id) AS rental_count
    FROM
        film
    JOIN
        inventory ON film.film_id = inventory.film_id
    JOIN
        rental ON inventory.inventory_id = rental.inventory_id
    GROUP BY
        film.film_id, film.title
    ORDER BY
        rental_count DESC
    LIMIT 5
)
, ExpectedAge AS (
    SELECT
        trm.film_id,
        trm.title,
        ROUND(AVG(EXTRACT(YEAR FROM current_date) - EXTRACT(YEAR FROM c.create_date))) AS expected_age
    FROM
        TopRentedMovies trm
    JOIN
        rental ON trm.film_id = rental.inventory_id
    JOIN
        customer ci ON rental.customer_id = ci.customer_id
    JOIN
        customer c ON ci.customer_id = c.customer_id
    GROUP BY
        trm.film_id, trm.title
)
SELECT
    trm.title AS "Top Rented Movies",
    ea.expected_age AS "Expected Age of Audience"
FROM
    TopRentedMovies trm
JOIN
    ExpectedAge ea ON trm.film_id = ea.film_id;
